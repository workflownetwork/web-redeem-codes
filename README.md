# README #

#### GENERAL NOTES.

Sample code for redeem code distribution.

### How it works ###

* Load in all your codes in the server_files/reedeemcodes.txt
* On the index.php page, a user enters a name and submits
* PHP will then iterate the next line in the redeemcodes.txt that does not have a names assigned.
* PHP then returns and posts with that code and writes the users inputted text to the txt file as a comma sperated array.
* Optional post to slack using [this slack app](https://slack.com/apps/A0F7XDUAZ-incoming-webhooks).

### Notes ###

* It us possible for a user to redeem a code more than once as there is no checking for duplicate name.
* Nothing is done with the user name / inputed text exept writig to text files and adding to slack.
* After a user clicks the "Click here to redeem" button, it then opens the AppStore to redeem. This can sometimes be slow.
