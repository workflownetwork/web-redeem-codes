<?php

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if (isset($_SESSION)) {
    session_destroy();
}
if (!isset($_SESSION)) {
    session_start();
}

// define variables and set to empty values
$name = $nameErr = $redeemCode = $redeemCodeText = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    postToSlack("cool");

    if (empty($_POST["name"])) {
        $nameErr = "Name is required";
    } else {
        $name = test_input($_POST["name"]);
        if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
            $nameErr = "Only letters and white space allowed";
        }
    }

    if (empty($nameErr)) {
        echo "do this.";
        $redeemCode = getRedeemCode($name);
        $redeemCodeText = $redeemCode;
        unset($_POST);
        header('Location: redeem.php?code=' . $redeemCode);
    } else {
        echo "name err?";
    }
}

function postToSlack($message) {
    //create a new cURL resource
    // use this app https://slack.com/apps/A0F7XDUAZ-incoming-webhooks
    $hookURL = "https://hooks.slack.com/services/xxxxxxxx/xxxxxxx/aaaaaaaaaaaaa";

    $ch = curl_init($hookURL);

    //setup request to send json via POST
    $data = array(
        'text' => $message,
    );
    $payload = json_encode($data);

    //attach encoded JSON string to the POST fields
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

    //set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

    //return response instead of outputting
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    //execute the POST request
    $result = curl_exec($ch);

    //close cURL resource
    curl_close($ch);
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function getRedeemCode($providedName) {
    echo "providedName = $providedName";

    // WARNING! Do not have this file in the doc locaion of your web server. Use a folder outside so the file is not accessable directly. 
    $redeemCodesFile = '/Actual_File_Location_On_Server/server_files/redeemcodes.txt';
    $data = file_get_contents($redeemCodesFile);
    
    $codesArray = explode("\n", $data);

    $firstUnusedCode = "";
    $newFileOutput = "";

    foreach ($codesArray as $dataSplit) {
        $arrayData = explode(',', $dataSplit);
        if (empty($arrayData[1])) {
            // No nane assigned to the code.
            if (empty($firstUnusedCode)) {
                $firstUnusedCode = $arrayData[0];
                $newFileOutput = $newFileOutput . $arrayData[0] . "," . $providedName;
            } else {
                $newFileOutput = $newFileOutput . $arrayData[0];
            }
        } else {
            $newFileOutput = $newFileOutput . $arrayData[0] . "," . $arrayData[1];
        }
        $newFileOutput = $newFileOutput . "\n";
    }

    // Write to file
    $handle = fopen($my_file, 'w') or die('Cannot open file:  ' . $redeemCodesFile);
    $writeData = $newFileOutput;
    fwrite($handle, $writeData);
    return $firstUnusedCode;
}
?>

<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
</head>
<body>

<h2>App Title - iOS Redeem Code</h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
Name: <input type="text" name="name">
  <span class="error">* <?php echo $nameErr; ?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">
</form>


</body>
<head>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
</head>
</html>
